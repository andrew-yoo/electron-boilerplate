# Electron Boilerplate Project

This is an attempt To understand how to put together an electron app which uses

* react
* typescript
* webpack + hot-reloading

There is already a good boilerplate app (  https://github.com/goshakkk/no-bs-react-webpack-starter ), but it had too many features that I didn't understand what they were doing.

I am building a boilerplate app, gradually adding all the features one by one to understand what each components do, and how they interact with each other.



## How does hot-reloading work?

1. WebPack DevServer is hosting and watching the content of app folder, and transpile the content into common javascript then notify the 'browser' whenever change is made.  
2. Electron is launched with environment variable HotReload=true, so that it will open the index.html hosted by the DevServer, instead of the one that exists locally in the filesystem.
3. In the published environment, it should refer to the one in the filesystem, not the one in the dev server
4. Environment Varialbes are set in NPM Script section in package.json file, with the help of NPM module called 'cross-env'


## What is left?

Still need to figure out

1. Packaging
2. AutoUpdate
3. CI
4. Test
5. Redux ( which should be straight forward; just treat this as normal react app )







#global dependencies

```
npm install -g webpack
```

```
npm install -g typescript tslint

```

```
npm install -g electron
```



#How to run


##Simple ( no hot reload )

```
npm start
```

## Hot Reload 

You will need 2 console command windows opened :

1. for the web-server which will host the resources ( it uses 3000 by default, if you need to change, update npm script 'hot-server' )
2. for launching electron


Console #1
```
npm run hot-server
```

Console #2
```
npm run hot-electron
```


# What resources were used?

## React
based on 
* https://github.com/goshakkk/no-bs-react-webpack-starter
* typescript tutorial
* and multiple boilerplate react+webpack apps

## Electron 
based on multiple sources,

* https://github.com/chentsulin/electron-react-boilerplate
* https://medium.com/@ad_harmonium/build-to-both-electron-js-and-browser-targets-using-webpack-59266bdb76a#.d8dbjb5oy
* and other boilerplates using react+electron
